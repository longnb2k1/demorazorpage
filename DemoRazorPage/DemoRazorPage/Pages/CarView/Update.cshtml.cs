using DemoRazorPage.Data;
using DemoRazorPage.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DemoRazorPage.Pages.CarView
{
    [BindProperties]
    public class UpdateModel : PageModel
    {
       
        private readonly ApplicationDBContext _db;
        public Car Car { get; set; }
        public UpdateModel(ApplicationDBContext db)
        {
            _db = db;
        }
        public void OnGet(int id)
        {

            //   Car = _db.Cars.Find(id);

           var ketqua = _db.Cars.FirstOrDefault(x=>x.CarId==id); 
            Car = ketqua;
        }
        public void OnPost()
        {
          
            Console.WriteLine("this is Post");
            Console.WriteLine(Car.CarId);
            Console.WriteLine(Car.Name);
            Console.WriteLine(Car.Manufacture);
            Console.WriteLine(Car.Price);
            Console.WriteLine(Car.releaseYear);

             _db.Update(Car);
            _db.SaveChanges();


            Response.Redirect("CarIndex");
        }
    }
}
