using DemoRazorPage.Data;
using DemoRazorPage.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DemoRazorPage.Pages.CarView
{
    public class DeleteModel : PageModel
    {
        private readonly ApplicationDBContext _db;
        public Car car { get; set; }
        public DeleteModel(ApplicationDBContext db)
        {
            _db = db;
        }
        public  void  OnGet(int id)
        {
            Console.WriteLine("this id is: " + id);
            car = _db.Cars.Find(id);
            if (ModelState.IsValid)
            {
                var carFromDb = _db.Cars.Find(car.CarId);
                if (carFromDb!=null)
                {
                    _db.Cars.Remove(carFromDb);
              
                    
                  _db.SaveChanges();

                }
                Response.Redirect("CarIndex");

            }
             
            
        }

      
    }
}
