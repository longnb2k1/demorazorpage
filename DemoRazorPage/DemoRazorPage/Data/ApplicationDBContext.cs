﻿using DemoRazorPage.Model;
using Microsoft.EntityFrameworkCore;

namespace DemoRazorPage.Data
{
    public class ApplicationDBContext : DbContext
    {

        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options): base(options)
        {

        }
        public DbSet<Car> Cars { get; set; }

    }
}
